package com.sky.besthardtool.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;


import com.sky.besthardtool.R;
import com.sky.besthardtool.base.ActionBarBaseA;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2017/12/27.
 */

public class WelcomeA extends ActionBarBaseA {

    Timer timer = new Timer();
    TimerTask task;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        task = new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(WelcomeA.this, CategoryA.class));
                WelcomeA.this.finish();
            }
        };

        timer.schedule(task, 2000);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        task.cancel();
        task = null;
        timer.cancel();
        timer = null;
    }
}
