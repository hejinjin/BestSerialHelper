package com.sky.besthardtool.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;


import com.sky.besthardtool.R;
import com.sky.besthardtool.base.ActionBarBaseA;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2018/3/9.
 *
 * @author Jin
 *         choose type
 *         include serialport Usb bluebooth
 */

public class CategoryA extends ActionBarBaseA {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        ButterKnife.bind(this);

    }


    @OnClick(R.id.rl_serial)
    void Serial_Click() {
        startActivity(new Intent(this,SerialConnectA.class));
    }

    @OnClick(R.id.rl_usb)
    void Usb_Click() {
        startActivity(new Intent(this,UsbA.class));
    }

    @OnClick(R.id.rl_bluetooth)
    void Bluetooth_Click() {
        startActivity(new Intent(this,BluetoothA.class));
    }

    @OnClick(R.id.rl_other)
    void Other_Click() {
        startActivity(new Intent(this,OtherA.class));
    }
}
