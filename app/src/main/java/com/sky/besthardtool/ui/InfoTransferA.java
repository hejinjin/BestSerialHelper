package com.sky.besthardtool.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.sky.besthardtool.R;
import com.sky.besthardtool.base.ActionBarBaseA;
import com.sky.besthardtool.category.serialport.listener.OnIOTransferListener;
import com.sky.besthardtool.category.serialport.manager.TransferManager;
import com.sky.besthardtool.category.serialport.service.InfoTransferS;
import com.sky.besthardtool.utils.ByteUtil;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 2017/12/27.
 */

public class InfoTransferA extends ActionBarBaseA {

    public InfoTransferS infoTransferS;
    public Intent intent;
    public InfoTransferS.InfoTransferBinder binder;

    @BindView(R.id.btn_init)
    Button btnInit;
    @BindView(R.id.btn_get_version)
    Button btnGetVersion;
    @BindView(R.id.btn_read)
    Button btnRead;
    @BindView(R.id.btn_write)
    Button btnWrite;
    @BindView(R.id.tv_log)
    TextView tvLog;
    private int type = 0;
    private String strLog = "";

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = (InfoTransferS.InfoTransferBinder) service;
            infoTransferS = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.v(this.getClass().getName(), "connect close!!!");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_transfer);
        ButterKnife.bind(this);

        intent = new Intent(InfoTransferA.this, InfoTransferS.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);


        TransferManager.getInstance().setOnIOTransferListener(new OnIOTransferListener() {
            @Override
            public void onReceive(final byte[] data, int size) {
                if (type == 0) {
                    strLog = "初始化";
                }

                if (type == 1) {
                    strLog = "获取版本号";
                }
                if (type == 2) {
                    strLog = "读数据";
                }
                if (type == 3) {
                    strLog = "写数据";
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvLog.setText(strLog + ":" + ByteUtil.byteArrayToString(data));
                    }
                });
                System.out.println("======>接收数据:" + ByteUtil.byteArrayToString(data));
            }

            @Override
            public void onSerialConnect(boolean isConnected) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(InfoTransferA.this, "连接失败,请选择别的串口", Toast.LENGTH_SHORT).show();
                        InfoTransferA.this.finish();
                    }
                });
            }
        });

//        btnSend = (Button) findViewById(R.id.btn_send);
//        btnSend.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    //AAFFCA02000F008857
//                    binder.sendCommand(ByteUtil.hexStr2Bytes("AA00016061BB"));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });

    }

    @OnClick(R.id.btn_init)
    public void Click_Init() {
        type = 0;
        try {
            //AAFFCA02000F008857
            binder.sendCommand(ByteUtil.hexStr2Bytes("AA00016061BB"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_get_version)
    public void Click_Get_Version() {
        type = 1;
        try {
            //AAFFCA02000F008857
            binder.sendCommand(ByteUtil.hexStr2Bytes("AA00016160BB"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_read)
    public void Click_Read() {
        type = 2;
        try {
            //AAFFCA02000F008857
            binder.sendCommand(ByteUtil.hexStr2Bytes("AA00056507086F0000BB"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_write)
    public void Click_Write() {
        type = 3;
        try {
            //AAFFCA02000F008857
            binder.sendCommand(ByteUtil.hexStr2Bytes("AA000D6607081122334455667788EC0000BB"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        TransferManager.getInstance().disConnect();
        unbindService(connection);
    }
}
