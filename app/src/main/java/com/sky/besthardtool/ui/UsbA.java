package com.sky.besthardtool.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;


import com.sky.besthardtool.R;
import com.sky.besthardtool.category.usb.base.BaseA;
import com.sky.besthardtool.category.usb.common.SampleCommand;

import butterknife.OnClick;

/**
 * Created by Administrator on 2018/3/9.
 */

public class UsbA extends BaseA {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usb);
    }


    @OnClick(R.id.btn_send)
    void Send_Click() {
        if (mDeviceConnection != null && isControlTrans) { //控制传输
            //发送
            ret = mDeviceConnection.controlTransfer(0x21, 0x09, 0x301, 0x00, outStream, outStream.length, 1000);
            if (ret >= 0) {
                //接收
                ret = mDeviceConnection.controlTransfer(0xA1, 0x01, 0x302, 0x00, inStream, inStream.length, 1000);
            }
        }

        if (mDeviceConnection != null && !isControlTrans) { //中断传输
            ret = mDeviceConnection.bulkTransfer(epOut, SampleCommand.COMMAND_, SampleCommand.COMMAND_.length, 1000);
            if (ret >= 0) {
                ret = mDeviceConnection.bulkTransfer(epIn, inStream, inStream.length, 1000);
            }
        }
    }
}
